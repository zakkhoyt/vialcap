// OpenSCAD cheat sheet: http://openscad.org/cheatsheet/
// Command line: https://files.openscad.org/documentation/manual/Using_OpenSCAD_in_a_command_line_environment.html

//
// (1) INCLUDES
// ======================================================================

use <threads.scad>

//
// (2) PARAMETERS
// ======================================================================

/* [Render Parameters] */
// ----------------------------------------------------------------------

// Render quality (fast preview does not render threads).
render_quality = "fast preview"; // ["fast preview", "preview", "final rendering"]
// render_quality = "preview"; // ["fast preview", "preview", "final rendering"]

// Which parts to render.
parts = "vial & cap (side by side)"; // ["vial", "cap", "vial & cap (closed)", "vial & cap (opened)", "vial & cap (side by side)"]

// TODO: convert to checkmark
// Center the vial on x/y axes.
centered = "yes"; // ["yes", "no"]

// TODO: convert to checkmark
// Expose a cross section of the parts. 
cross_section = "no"; // ["yes", "no"]

/* [Linking] */
// ----------------------------------------------------------------------

// Add more threads to bottom of vial and top of cap so that they can be screwed together (with or without the lid). If linkable, no holes will be made in the cap.
linkable = "no"; // ["yes", "no"]

/* [Measurement Parameters] */
// ----------------------------------------------------------------------

// The total height of the vial and cap when assembled. [mm]
total_h = 30; 
echo(str("total_h: ", total_h));

// The total width of the vial or cap. [mm]
total_w = 20;
// total_w = 34; // 18650 case
echo(str("total_w: ", total_w));

// Widen the inner diameter below the threaded section as permitted by the configured minimum wall thickness.
inner_dia_widen = true;

// inner_dia_widen = false; // 18650;
echo(str("inner_dia_widen: ", inner_dia_widen));

// cap height, as fraction of total outer height. [%]
cap_h_percent = 30;
echo(str("cap_h_percent: ", cap_h_percent));

// Default wall thickness. Used for all walls, except the side walls when "inner dia widen" is unchecked. [mm]
wall_t = 1.8;
echo(str("wall_t: ", wall_t));

// cap turns to close resp. open it. (Note: "2" is comfortable, as used on soda vials). 
cap_turns = 2; //4;
echo(str("cap_turns: ", cap_turns));

// Number of thread starts.
thread_starts = 2; //1;
echo(str("thread_starts: ", thread_starts));

/* [Nerd Parameters] */

// Letters for the cap
cap_text = "";

// The number of cuts into the cap. 
cutter_count = 15;

// The width of the cuts into the cap.
cutter_width = 2;

// Height of the cuts into the cap. Value of -1 will use the cap height. [mm]
cutter_height = -1;

// The depth of the cuts into the cap. [mm]
cutter_depth = 2;

// The radius of the salt holes. 
hole_radius = 1;

// The number of salt holes. 
hole_count = 0;

// The offset of the salt holes from the center. Value of < 0 will use 1/2 of the radius. [mm]
hole_offset = -1;


/* [Hidden] */
// ----------------------------------------------------------------------
// (This is a special section that OpenSCAD will not show in the 
// customizer. Used for private variables. 

// Fragment number in a full circle. (Threads set their own number.)
$fn = (render_quality == "final rendering") ? 120 :
      (render_quality == "preview") ? 60 : 
      (render_quality == "fast preview") ? 30 : 30; // Same as the default $fa == 12.

// Wether or not to render threads
thread_testmode = (render_quality == "fast preview") ? true : false;

// TODO: aka: total_h
outer_h = total_h;
echo(str("outer_h: ", outer_h));

inner_h = outer_h - 2*wall_t;
echo(str("inner_h: ", inner_h));

cap_h = outer_h * cap_h_percent/100;
echo(str("cap_h: ", cap_h));

vial_h = outer_h - cap_h;
echo(str("vial_h: ", vial_h));

outer_r = total_w / 2;
echo(str("outer_r: ", outer_r));

// Angle between one thread-"V" flank and the orthogonal of the thread axis.
// Using 45° for printability.
thread_angle = 45;
echo(str("thread_angle: ", thread_angle));

thread_h = cap_h - wall_t;
echo(str("thread_h: ", thread_h));

thread_turns = cap_turns * thread_starts;
echo(str("thread_turns: ", thread_turns));

// Thread pitch, as expected by the threads.scad library.
// 
// Pitch as "axial travel per turn" is independent of the number of 
// thread starts: an additional thread start just generates another 
// parallel set of turns, "in the groove of an the existing turn", 
// so using the same pitch.
//   So normally we'd have to calculate it as "thread_h / cap_turns".
// However threads.scad has a bug and for multi-start threads 
// expects here only the portion of pitch that is in addition to the 
// pitch enforced by the other turns of the thread.
// 
// TODO Fix the threads.scad bug described above and then calculate 
// with "thread_h / cap_turns".
// TODO Generate a warning when the pitch is not small enough to 
// ensure proper fastening of the cap. For comparison, a commercial 
// soda vial has 5 mm thread pitch.
thread_pitch = thread_h / thread_turns;
echo(str("thread_pitch: ", thread_pitch));

// TODO: This may not be totally exact as the thread grooves at the 
// start and end of the thread might miss the "flattened ridge" and 
// "flattened valley" parts of their thread profile. So for 5 nominal 
// thread turns, only 4.7 or something might be present.
thread_turn_h = thread_h / thread_turns;
echo(str("thread_turn_h: ", thread_turn_h));

// Depth of the thread grooves if the thread would have pointed ridges 
// and valleys.
thread_t_sharp = (thread_turn_h / 2) / sin(thread_angle);
echo(str("thread_t_sharp: ", thread_t_sharp));

// Depth of the external and internal thread grooves in practice, as 
// they are not sharp. See h, h_fac1, h_fac2 in threads.scad.
thread_t_external = thread_t_sharp * 0.6625;
echo(str("-- thread_t_external: ", thread_t_external));

thread_t_internal = thread_t_sharp * 0.625;
echo(str("-- thread_t_internal: ", thread_t_internal));

delta_thread_t = thread_t_external - thread_t_internal;
echo(str("-- delta_thread_t: ", delta_thread_t));

// Radial air gap between inner and outer thread for printability.
// - Good fit on a 6 mm pitch, 30° flank angle thread: 1.25 mm
// - On a commercial soda vial@ ~0.5 mm
// TODO Calculate a suitable value based on the amount of overlap 
// in the thread (see thread_t_*).
// TODO Create a warning when the required value will be hard to print 
// with standard settings (200-300 µm layer height) and recommend 
thread_gap = thread_t_internal * 0.5;
// thread_gap = delta_thread_t * 5;
echo(str("-- thread_gap: ", thread_gap));

inner_r = (total_w - 2.0 * wall_t) / 2;
echo(str("inner_r: ", inner_r));
assert(inner_r < total_w / 2);

vial_thread_outer_r = inner_r - (thread_t_external + thread_gap);
echo(str("vial_thread_outer_r: ", vial_thread_outer_r));
assert(vial_thread_outer_r < inner_r);

vial_thread_inner_r = vial_thread_outer_r - thread_t_internal;
echo(str("vial_thread_inner_r: ", vial_thread_inner_r));
assert(vial_thread_inner_r < vial_thread_outer_r);

inner_min_r = vial_thread_inner_r - wall_t;
echo(str("inner_min_r: ", inner_min_r));
assert(inner_min_r < vial_thread_inner_r);

cap_thread_outer_r = inner_r - (thread_t_external);
echo(str("cap_thread_outer_r: ", cap_thread_outer_r));

// Outer radius based on the components in the cap section, from inside out.
// 
// On delta_thread_t: It seems that the nominal diameter to be given to metric_thread() 
// for an internal thread is the outer diameter of an external thread 
// that can be screwed in with zero gap between thread turns. Now the 
// valley on an internal thread is a bit deeper than the ridge of an 
// external thread is high (and both are not pointed), leaving a small 
// gap. This gap is what we have to compensate for here with "+ delta_thread_t".
// Not sure if it's really simply this difference of thread depth to use, 
// but it is visually exact for the thread analyzed. Without this, the 
// wall would become thinner than wall_t.
// TODO: Once being sure that this explanation applies, rather make the 
// fix by converting from real to expected nominal thread diameter when 
// calling metric_thread() to generate the internal thread. And use the 
// real thread diameter elsewhere: cap_thread_outer_r = vial_thread_outer_r + thread_gap + delta_thread_t;
// outer_r = cap_thread_outer_r + delta_thread_t + wall_t;

// TODO: zakk expose chamfer_t as chamfer_height_percent
// Radial width of chamfers around all non-threaded circular edges.
// (The threads generate their own chamfers via parameter "leadin".)
chamfer_t = wall_t * 0.6;
echo(str("chamfer_t: ", chamfer_t));

// Small amount of overlap for joins and extended cuts to prevent z-fighting.
nothing = 0.01;
echo(str("nothing: ", nothing));
echo(
    str(
        "Suggested file name: ",
        total_h,
        "_",
        total_w,
        "_",
        inner_dia_widen,
        "_",
        cap_h_percent,
        "_",
        wall_t,
        "_",
        cap_turns,
        "_",
        thread_starts,
        ".stl"
    )
);

//
// (3) UTILITY FUNCTIONS
// ======================================================================

// Cuboid cutters arranged in a circle.
//   radius: Circle radius on which to center the serration cutters.
//   cutter_count: Number of cutters to use.
//   cutter_width, cutter_depth, cutter_height: Dimensions of one cuboid cutter, aligned 
//     with their x axis tangentially to the circle to cut into, and centered on the circle.
module serrate(
    radius,
    cutter_count,
    cutter_depth,
    cutter_width,
    cutter_height
) {
    for (i = [0 : cutter_count-1]) {
        translate ([
            radius*cos(i*(360/cutter_count)), 
            radius*sin(i*(360/cutter_count)),
            0
        ]) {
            rotate([0, 0, i*(360/cutter_count)]) {
                cube([cutter_depth, cutter_width, cutter_height], center=true);
            }
        }
    }
}

//
// (4) PART GEOMETRIES
// ======================================================================

// solid shape of the cylindrical part of the vial (without the threaded section)
module main_vial_solid() {
    echo(str("container(): creating main container vial: cylinder(r=", outer_r, "h=", vial_h ,")"));

    difference() {
        translate([0, 0, vial_h/2]) {
            cylinder(r=outer_r, h=vial_h, center=true);
        }
            
        
        // Chamfer around the top (45°).
        translate([0, 0, -(chamfer_t/2) + vial_h + nothing]) {
            difference() {
                cylinder(r=outer_r+nothing, h=chamfer_t, center=true);
                cylinder(r1=outer_r+nothing, r2=outer_r-chamfer_t, h=chamfer_t + 2*nothing, center=true);
            }
        }
        
        if (linkable != "yes"){
            // Chamfer around the bottom (45°).
            translate([0, 0, chamfer_t/2 - nothing]) {
                difference() {
                    cylinder(
                        r=outer_r+nothing, 
                        h=chamfer_t, 
                        center=true
                    );
                    cylinder(
                        r1=outer_r-chamfer_t, 
                        r2=outer_r+nothing, 
                        h=chamfer_t + 2*nothing, 
                        center=true
                    );
                }
            }
        }    
    }
}

module vial_body() {
    difference() {
        // solid outer shape of the container.
        color("SteelBlue"){
            union() {
                main_vial_solid();
                translate([0, 0, vial_h - nothing]) {
                    vial_thread();
                }
            }
        }

        // Cutout that makes the container hollow.
        color("Chocolate") {
            union() {
                // Cylindrical cutout for the inside. This is everything if inner_dia_widen == false.
                translate([0, 0, inner_h/2 + wall_t + nothing]) {
                    cylinder(r=inner_min_r, h=inner_h, center=true);
                }

                // Widen the cutout in the non-threaded section so that side walls are only wall_t thick.
                if (inner_dia_widen) {
                    inner_max_r = outer_r - wall_t;
                    echo(str("inner_max_r: ", inner_max_r));
                    // TODO: zakk expose bevel_divisor to customizer?
                    bevel_divisor = 1;
                    echo(str("bevel_divisor: ", bevel_divisor));
                    
                    delta_r = (inner_max_r - inner_min_r) / bevel_divisor;
                    echo(str("delta_r: ", delta_r));

                    // bevel_inner_r = inner_min_r + delta_r;
                    bevel_inner_r = inner_max_r - delta_r;
                    echo(str("bevel_inner_r: ", bevel_inner_r));
                                    
                    // Main cutout, for the bottom (=non-threaded) section. 
                    max_r_cutout_h = vial_h - 2*wall_t - delta_r;
                    echo(str("max_r_cutout_h: ", max_r_cutout_h));

                    // "- 2*wall_t" as this section has a bottom wall and its own top wall.
                    // "- delta_r" to make space for the inner 45° chamfer.
                    translate([0, 0, max_r_cutout_h / 2 + wall_t + nothing]) {
                        cylinder(r=inner_max_r, h=max_r_cutout_h, center=true);
                    }
                    
                    // Concical cutout for a ≤45° angle between narrower threaded and wider 
                    // non-threaded section. Needed for printability without support.
                    translate([0, 0, -delta_r/2 + vial_h - wall_t]){
                        cylinder(r1=inner_max_r, r2=bevel_inner_r, h=delta_r, center=true);
                    }
                }

                // A note to say how wide the interior is
                if (inner_dia_widen) {
                    // A note to say how wide the interior is
                    total_wall_t = 2 * (wall_t);
                    echo(str("! ---- total_wall_t: ", total_wall_t));

                    volume_d = total_w - total_wall_t;
                    echo(str("! ---- volume_d: ", volume_d));

                    volume_h = total_h - (2 * wall_t);
                    echo(str("! ---- volume_h: ", volume_h));

                    // TODO: zakk finish this
                    // volume = 2 * PI * (volume_d / 2) * volume_h;
                    // echo(str("! ---- volume: ", volume, " mm^3"));
                    // echo(str("! ---- volume: ", volume / 1000, " cm^3"));
                } else {
                    // A note to say how wide the interior is
                    total_wall_t = 2 * (thread_t_external + thread_gap + thread_t_internal + wall_t + wall_t);
                    echo(str("total_wall_t: ", total_wall_t));

                    volume_d = total_w - total_wall_t;
                    echo(str("! ---- volume_d: ", volume_d));

                    volume_h = total_h - (2 * wall_t);
                    echo(str("! ---- volume_h: ", volume_h));

                    volume = 2 * PI * (volume_d / 2) * volume_h;
                    echo(str("! ---- volume: ", volume, " mm^3", " or ", volume / 1000, " cm^3"));
                }
            }
        }
    }
}

module vial() {
    union() {
        echo("--- linkable: ", linkable);
        if (linkable == "yes") {
            echo("--- aa: ");
            cap(suppress_top_threads=true);
            translate([0, 0, cap_h]) {
                vial_body();
            }
        } else {
            echo("--- bb: ");
            vial_body();
        }
    }
}

module vial_thread() {
    difference() {
        // solid outer shape of the container.
        color("SteelBlue") {
            union() {
                // Threaded section, with thread.
                // translate([0, 0, vial_h - nothing])
                metric_thread(
                    diameter=2*vial_thread_outer_r, 
                    pitch=thread_pitch, 
                    length=thread_h + nothing, 
                    n_starts=thread_starts, 
                    leadin=1, 
                    angle=thread_angle, test=thread_testmode
                );
            }
        }

        // Cutout that makes the container hollow.
        color("#00FFFF") {
            union() {
                // Cylindrical cutout for the inside. This is everything if inner_dia_widen == false.
                translate([0, 0, inner_h/2 + wall_t + nothing]) {
                    cylinder(r=inner_min_r, h=inner_h, center=true);
                }
            }
        }
    }
}

// solid base shape of the cap, incl. chamfers.
module cap_solid(suppress_cuts) {
    difference() {
        // Cylindrical base shape.
        translate([0, 0, cap_h/2]){
            cylinder(r=outer_r, h=cap_h, center=true);
        }
    
        if (!suppress_cuts) {
            // Chamfer around the top (45°).
            translate([0, 0, -(chamfer_t/2) + cap_h + nothing]) {
                difference() {
                    cylinder(
                        r=outer_r+nothing, 
                        h=chamfer_t, 
                        center=true
                    );
                    cylinder(
                        r1=outer_r+nothing, 
                        r2=outer_r-chamfer_t, 
                        h=chamfer_t + 2*nothing, 
                        center=true
                    );
                }
            }
        }    
        
        // Chamfer around the bottom (45°).
        translate([0, 0, chamfer_t/2 - nothing]) {
            difference() {
                cylinder(
                    r=outer_r+nothing, 
                    h=chamfer_t, 
                    center=true
                );
                cylinder(
                    r1=outer_r-chamfer_t, 
                    r2=outer_r+nothing, 
                    h=chamfer_t + 2*nothing, 
                    center=true
                );
            }
        }
    }
}

module cap_body(suppress_cuts) {
    function cosr(x) = cos(180 * x / PI);
    function sinr(y) = sin(180 * y / PI);
    difference() {
        color("SteelBlue"){
            // cap_solid();
            cap_solid(suppress_cuts = suppress_cuts);
        }

        // Internal thread, drilled into the cap.

        // Workaround for a bug in threads.scad that causes z-fighting in preview mode when applying this chamfer.
        leadin = (render_quality == "final rendering") ? 3 : 0; 
        color("Chocolate") {
            translate([0, 0, -nothing]){
                metric_thread(
                    diameter=2*cap_thread_outer_r,
                    pitch=thread_pitch,
                    length=thread_h + nothing,
                    internal=true,
                    n_starts=thread_starts,
                    leadin=leadin,
                    angle=thread_angle,
                    test=thread_testmode
                );
            }
        }
        
        if (!suppress_cuts) {
            // Grip surface around the cap.
            color("Chocolate") {
                translate([0, 0, cap_h/2])
                    serrate(
                        radius=outer_r, 
                        cutter_count=15, 
                        cutter_width=cutter_width, 
                        cutter_depth=cutter_depth, 
                        cutter_height=cutter_height < 0 ? cap_h+2*nothing : cutter_height
                    );

                // Salt holes (avoid divide by 0 error)
                if (hole_count > 0) {
                    safe_hole_offset = hole_offset < 0 ? inner_r / 2 : hole_offset;
                    for (i = [0 : hole_count]) {
                        translate([
                            cosr((i / hole_count) * 2 * PI) * safe_hole_offset, 
                            sinr((i / hole_count) * 2 * PI) * safe_hole_offset, 
                            cap_h - wall_t / 2
                        ]) {
                            cylinder(
                                r = linkable == "yes" ? 0 : hole_radius, 
                                h = 2 * wall_t, 
                                center = true
                            );    
                        }
                    }
                }

                translate([0, 0, cap_h]) {
                    scale(v = 0.2){
                        text(cap_text, halign="center", valign="center");
                    }
                }
            }
        }
    }
}

module cap(suppress_top_threads) {
    union() {
        echo("--- suppress_top_threads: ", suppress_top_threads);
        if (is_undef(suppress_top_threads) && linkable == "yes") {
            echo("--- cc: ");
            // Regular cap w/threads added to the top
            translate([0, 0, cap_h]) {
                vial_thread();
            }
            cap_body();
        } else if (linkable == "yes") {
            echo("--- dd: ");
            // Cap for bottom of vial (no outer cuts)
            cap_body(suppress_cuts=true);
        } else {
            echo("--- ee: ");
            // Regular cap
            cap_body();
        }
    }
}

//
// (5) ASSEMBLY
// ======================================================================

module main () {
    module mask_cross_section() {
        if (cross_section == "yes") {
            translate([
                centered == "yes" ? 0 : 0 + outer_r,  
                centered == "yes" ? -2 * total_w : -2 * total_w + outer_r, 
                centered == "yes" ? -nothing : -nothing  
            ])
            cube([4 * total_w, 4 * total_w, 2 * total_h], center=false);
        }
    }

    // c_w = 18;
    // #translate([-c_w / 2, -c_w / 2, wall_t]) {
    //     cube([c_w, c_w, 65]);
    // } 

    if (parts == "vial") {
        difference() {
            translate([
                centered == "yes" ? 0 : outer_r, 
                centered == "yes" ? 0 : outer_r, 
                0
            ])
            vial();
            mask_cross_section();
        }
    } else if (parts == "cap") {
        difference() {
            translate([
                centered == "yes" ? 0 : outer_r, 
                centered == "yes" ? 0 : outer_r, 
                cap_h
            ])
            rotate([180, 0, 0])
            cap();
            
            mask_cross_section();
        }
    } else if (parts == "vial & cap (closed)") {
        difference() {
            // Container with the cap screwed.
            union() {
                translate([
                    centered == "yes" ? 0 : outer_r, 
                    centered == "yes" ? 0 : outer_r, 
                    0
                ])
                vial();

                // Position the lid screwed in one turn. Useful to debug the thread gap.
                // TODO Make this work also for non-integer lid_turns values, by also 
                // rotating the lid by the fractional amount.
                raise_lid_h = thread_h - thread_turn_h;

                // TODO: zakk: Another option for fully closed vs lightly closed?
                // cap_translate_z = vial_h + raise_lid_h;
                cap_translate_z = vial_h + thread_gap / 2;
                translate([
                    centered == "yes" ? 0 : outer_r, 
                    centered == "yes" ? 0 : outer_r, 
                    linkable == "yes" ? cap_translate_z + cap_h : cap_translate_z
                ])
                cap();
            }
            
            mask_cross_section();
        }
    } else if (parts == "vial & cap (opened)") {
        difference() {
            // Container with the cap unscrewed.
            union() {
                translate([
                    centered == "yes" ? 0 : outer_r, 
                    centered == "yes" ? 0 : outer_r, 
                    0
                ])
                vial();

                cap_translate_z = vial_h + 2 * cap_h;
                translate([
                    centered == "yes" ? 0 : outer_r, 
                    centered == "yes" ? 0 : outer_r, 
                    linkable == "yes" ? cap_translate_z + cap_h : cap_translate_z
                ])
                cap();
            }
            
            mask_cross_section();
        }
    } else if (parts == "vial & cap (side by side)") {
        difference() {
            // Container with the cap unscrewed.
            union() {
                translate([
                    centered == "yes" ? 0 : outer_r, 
                    centered == "yes" ? 0 * outer_r : 0 * outer_r, 
                    0
                ])
                vial();

                if (linkable == "yes") {
                    translate([
                        centered == "yes" ? 0 : outer_r, 
                        centered == "yes" ? 2.5 * outer_r : 2.5 * outer_r, 
                        0
                    ])
                    cap();
                } else {
                    translate([
                        centered == "yes" ? 0 : outer_r, 
                        centered == "yes" ? 2.5 * outer_r : 2.5 * outer_r, 
                        cap_h
                    ]) {
                        rotate([180, 0, 0]) {
                            cap();
                        }
                    }
                }
            }
            
            mask_cross_section();
        }
    }
}

main();
